<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
        <title>JSP - Login</title>
        <style>
            body {
                background-color: #91121b;;

            }
            .login-form {

                width: 340px;
                margin: 50px auto;
                margin-top: 150px;

            }

            .login-form form {
                color: white;
                margin-bottom: 15px;
                background: #1f1f1f;
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
                padding: 30px;
            }

            .login-form h2 {
                margin: 0 0 15px;
            }

            .form-control,
            .btn {
                min-height: 38px;
                border-radius: 2px;
            }

            .btn {
                font-size: 13px;
                font-weight: bold;
                width: 280px;
                height:35px;
                background:   #91121b;


            }

        </style>
    </head>

<body>
<div class="login-form">
    <form  method="post">
        <h2 class="text-center">LOGIN</h2>
        <div class="form-group">
            <input  type="text" class="form-control" placeholder="Email" required="required">

        </div>
        <div class="form-group">
            <input  type="password" class="form-control" placeholder="Senha" required="required">

        </div>


        <div class="form-group">
            <button type="button" class="btn btn-outline-dark"href="">Entrar</button>

        </div>



</div>

<br/>

</body>
</html>